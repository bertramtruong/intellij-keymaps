intellij-keymaps
===============================

IntelliJ Keymaps

### IntelliJ

- Windows `<User home>\.IntelliJIdea13\config\keymaps`
- Linux `~/.IntelliJIdea13/config/keymaps`
- Mac OS `~/Library/Preferences/IntelliJIdea13/keymaps`

### PhpStorm 7

- Windows `<User home>\.WebIde70\config\keymaps`
- Linux `~/.WebIde70/config/keymaps`
- Mac OS `~/Library/Preferences/WebIde70/keymaps`

## Easy Install Commands

### Linux

#### IntelliJ IDEA 13

- `mv ~/.IntelliJIdea13/config/keymaps ~/.IntelliJIdea13/config/keymaps_back`
- ``ln -s `pwd`/keymaps ~/.IntelliJIdea13/config/keymaps``

#### IntelliJ IDEA 14

- `mv ~/.IntelliJIdea14/config/keymaps ~/.IntelliJIdea14/config/keymaps_back`
- ``ln -s `pwd`/keymaps ~/.IntelliJIdea14/config/keymaps``

### Mac OS X

#### Clion
- `mv ~/Library/Preferences/clion10/keymaps ~/Library/Preferences/clion10/keymaps_back`
- ``ln -s `pwd`/keymaps ~/Library/Preferences/clion10/keymaps``

#### IntelliJ IDEA 13
- `mv ~/Library/Preferences/IntelliJIdea13/keymaps ~/Library/Preferences/IntelliJIdea13/keymaps_back`
- ``ln -s `pwd`/keymaps ~/Library/Preferences/IntelliJIdea13/keymaps``

#### IntelliJ IDEA 14
- `mv ~/Library/Preferences/IntelliJIdea14/keymaps ~/Library/Preferences/IntelliJIdea14/keymaps_back`
- ``ln -s `pwd`/keymaps ~/Library/Preferences/IntelliJIdea14/keymaps``
